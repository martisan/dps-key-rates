using LinearAlgebra
using JuMP
using Mosek
using MosekTools
using FastGaussQuadrature
using QuantumInformation
using Optim
using MathOptInterface


"""
    SecurityParameters

The desired security parameters.
"""
struct SecurityParameters
    eps_s::Float64      # Smoothing parameter of smooth min-entropy
    eps_PA::Float64     # PA parameter (exponential term in the leftover hashing Lemma)
    eps_EC::Float64     # Probability that the two keys differ if EC did not abort
    eps_EC_com::Float64 # Abort probability due to error in EC
end


"""
    MinTradeoff

A min-tradeoff function parametrized by c_λ and gradient λ.
"""
struct MinTradeoff
    c_λ::Float64
    λ::Vector{Float64}
end


"""
    ProtocolParameters

The parameters to the protocol.
"""
struct ProtocolParameters
    n::Int              # Number of protocol rounds
    γ::Float64          # Testing probability
    α::Float64          # Amplitude of the laser light
    p0::Vector{Float64} # Expected values of the statistical check
    trdf::MinTradeoff   # Min tradeoff function (the parameter CA in the paper)
end


function ProtocolParameters(;
    n::Int,
    γ::Float64,
    α::Float64,
    p0::Vector{Float64},
    trdf::MinTradeoff
)
    return ProtocolParameters(n, γ, α, p0, trdf)
end


"""
    min_tradeoff_props(params::ProtocolParameters)

Compute the properties of the min-tradeoff function.
"""
function min_tradeoff_props(params::ProtocolParameters)
    trdf = params.trdf

    max_g = trdf.c_λ + maximum(trdf.λ)
    min_g = trdf.c_λ + minimum(trdf.λ)
    γ = params.γ

    return (
        min=(1 - 1 / γ) * max_g + min_g / γ,
        min_sigma=min_g,
        max=max_g,
        var=(max_g - min_g)^2 / γ,
    )
end


"""
    quad_weights(m)

Weights of Gauss-Radau quadrature of order `m`.
"""
function quad_weights(m::Int)
    t, w = gaussradau(m)
    (1 .- reverse(t)) / 2, reverse(w) / 2
end


"""
    min_tradeoff(α, γ, λ, mmts; m=8)

Compute c_λ for a given gradient `λ`.
"""
function min_tradeoff(α, γ, λ; m::Int=8)
    # The dimensions
    dim_A = 2
    dim_B = 2 * 2 # Dimensions in the Choi Isomorphism
    dim_AB = dim_A * dim_B

    model = Model(optimizer_with_attributes(Mosek.Optimizer, "QUIET" => true))
    ts, ws = quad_weights(m)

    ##### Build the variables #####
    # The Choi state
    @variable(model, rho_cj[1:dim_AB, 1:dim_AB], PSD)
    rho_cj = convert(Matrix{AffExpr}, rho_cj)

    # Compute the channel from the Choi state
    E = inv_choi_isom(convert(Matrix{AffExpr}, rho_cj), dim_A, dim_B)

    # Apply the channel
    phi_in = phi_VT(α)
    chan = channel_kron(Matrix(I, 2^2, 2^2), E)
    rho = mat(chan * vec(phi_in))

    zetas = Matrix{AffExpr}[]
    etas = Matrix{AffExpr}[]
    thetas = Matrix{AffExpr}[]
    for i in 1:m, a in 1:2
        M, N = size(rho)
        zeta_i = @variable(model, [1:M, 1:N])
        push!(zetas, zeta_i)

        eta_i = @variable(model, [1:M, 1:N], PSD)
        push!(etas, eta_i)

        theta_i = @variable(model, [1:M, 1:N], PSD)
        push!(thetas, theta_i)
    end
    zetas = reshape(zetas, m, 2)
    etas = reshape(etas, m, 2)
    thetas = reshape(thetas, m, 2)


    ##### Build the objective #####
    obj = 0
    mmt_ops = [mmt(1, 1) + mmt(1, 2), mmt(2, 1) + mmt(2, 2)]
    op_A_tot = sum(mmt_ops)
    for i in 1:m, a in 1:2
        op_A = mmt_ops[a]
        op = op_A * rho + op_A * (zetas[i, a] + zetas[i, a]' + (1 - ts[i]) * etas[i, a]) + ts[i] * op_A_tot * thetas[i, a]
        obj += ws[i] * tr(op) / (ts[i] * log(2))
    end

    mmts = [constraint_mmt(i) for i in 1:3]
    delta::AffExpr = 0
    for (mmt, λ_i) in zip(mmts, λ)
        delta += λ_i * tr(rho * mmt)
    end
    @objective(model, Min, (1 - γ) * obj - delta)


    ##### Add the constraints #####
    # The constraints on the Choi matrix
    @constraint(model, tr(rho_cj) == 1)

    # The non-signaling constraints on the Choi matrix
    dims = [2, 2, 2]
    lhs = ptrace(rho_cj, dims, 2)
    rhs = kron(mixed(2), ptrace(rho_cj, dims, [1, 2]))
    @constraint(model, lhs .== rhs)

    # The PSD constraints
    for i in 1:m, a in 1:2
        Gamma1 = [rho zetas[i, a]'; zetas[i, a] thetas[i, a]]
        @constraint(model, Gamma1 in PSDCone())
        Gamma2 = [rho zetas[i, a]; zetas[i, a]' etas[i, a]]
        @constraint(model, Gamma2 in PSDCone())
    end

    # Solve the SDP
    optimize!(model)
    return dual_objective_value(model)
end


"""
    compute_grad(α; stats, m::Int=8)

Compute the entropy and gradient at the point `stats`.
"""
function compute_grad(α; stats, m::Int=8)
    mmts = [constraint_mmt(i) for i in 1:3]
    @assert length(stats) == length(mmts)

    # The dimensions
    dim_A = 2
    dim_B = 2 * 2 # Dimensions in the Choi Isomorphism
    dim_AB = dim_A * dim_B

    model = Model(optimizer_with_attributes(Mosek.Optimizer, "QUIET" => true))
    ts, ws = quad_weights(m)


    ##### Build the variables #####
    # The Choi state
    @variable(model, rho_cj[1:dim_AB, 1:dim_AB], PSD)
    rho_cj = convert(Matrix{AffExpr}, rho_cj)

    # Compute the channel from the Choi state
    E = inv_choi_isom(convert(Matrix{AffExpr}, rho_cj), dim_A, dim_B)

    # Apply the channel
    phi_in = phi_VT(α)
    chan = channel_kron(Matrix(I, 2^2, 2^2), E)
    rho = mat(chan * vec(phi_in))

    zetas = Matrix{AffExpr}[]
    nus = Matrix{AffExpr}[]
    thetas = Matrix{AffExpr}[]
    for i in 1:m, a in 1:2
        M, N = size(rho)

        Gamma1 = @variable(model, [1:2M, 1:2N], PSD)
        Gamma2 = @variable(model, [1:2M, 1:2N], PSD)
        @constraint(model, Gamma1[1:M, 1:N] .== rho)
        @constraint(model, Gamma2[1:M, 1:N] .== rho)
        @constraint(model, Gamma1[1:M, (N+1):end] .== Gamma2[(M+1):end, 1:N])

        zeta_i = Gamma1[1:M, (N+1):end]
        push!(zetas, zeta_i)

        nu_i = Gamma1[(M+1):end, (N+1):end]
        push!(nus, nu_i)

        theta_i = Gamma2[(M+1):end, (N+1):end]
        push!(thetas, theta_i)
    end
    zetas = reshape(zetas, m, 2)
    nus = reshape(nus, m, 2)
    thetas = reshape(thetas, m, 2)


    ##### Build the objective #####
    obj = 0
    mmt_ops = [mmt(1, 1) + mmt(1, 2), mmt(2, 1) + mmt(2, 2)]
    op_A_tot = sum(mmt_ops)
    for i in 1:m, a in 1:2
        op_A = mmt_ops[a]
        op = op_A * rho + op_A' * (zetas[i, a] + zetas[i, a]' + (1 - ts[i]) * nus[i, a]) + ts[i] * op_A_tot * thetas[i, a]
        obj += ws[i] * tr(op) / (ts[i] * log(2))
    end
    @objective(model, Min, obj)


    ##### Add the constraints #####
    # The constraints on the Choi matrix
    @constraint(model, tr(rho_cj) == 1)

    # The non-signaling constraints on the Choi matrix
    dims = [2, 2, 2]
    lhs = ptrace(rho_cj, dims, 2)
    rhs = kron(mixed(2), ptrace(rho_cj, dims, [1, 2]))
    @constraint(model, lhs .== rhs)

    # The measurement constraints
    constraints = ConstraintRef[]
    for (mmt, p) in zip(mmts, stats)
        con_ref = @constraint(model, tr(mmt * rho) <= p)
        push!(constraints, con_ref)
    end

    # Solve the SDP
    optimize!(model)
    λ = shadow_price.(constraints)
    return dual_objective_value(model), λ
end


"""
    inv_choi_isom(ρ, dim_A, dim_B)

Apply the inverse of the Choi-Jamiołkowski isomorphism.
"""
function inv_choi_isom(rho::Matrix{T}, dim_A::Int, dim_B::Int) where {T}
    m, n = size(rho)
    @assert m == n
    @assert dim_A * dim_B == m

    tmp1 = reshape(rho, (dim_B, dim_A, dim_B, dim_A))
    tmp2 = PermutedDimsArray(tmp1, [1, 3, 2, 4])
    return dim_A * reshape(tmp2, (dim_B^2, dim_A^2))
end


"""
    mixed(dim::Int)

The fully mixed state of dimension `dim`.
"""
mixed(dim::Int) = Matrix(I, dim, dim) / dim


"""
    mat(v::Vector)

Reshape a vector into a square matrix. This is the inverse to `vec`.
"""
function mat(v::Vector{T}) where {T}
    n = length(v)
    m = isqrt(n)
    @assert m^2 == n

    return reshape(v, (m, m))
end


"""
    channel_kron(E, F)

Tensor product of two channels.
"""
function channel_kron(E::Matrix{T}, F::Matrix{U}) where {T,U}
    m_E, n_E = size(E)
    dim_A1, dim_A2 = isqrt(n_E), isqrt(m_E)
    @assert m_E == dim_A2^2 && n_E == dim_A1^2
    tmp_E = reshape(E, (dim_A2, dim_A2, dim_A1, dim_A1))

    m_F, n_F = size(F)
    dim_B1, dim_B2 = isqrt(n_F), isqrt(m_F)
    @assert m_F == dim_B2^2 && n_F == dim_B1^2
    tmp_F = reshape(F, (dim_B2, dim_B2, dim_B1, dim_B1))

    # Build the tensored channel
    res = zeros(promote_type(T, U), m_E * m_F, dim_B1, dim_A1, dim_B1, dim_A1)
    for i_A in 1:dim_A1, j_A in 1:dim_A1
        for i_B in 1:dim_B1, j_B in 1:dim_B1
            m1 = tmp_E[:, :, i_A, j_A] # E(|i_A><j_A|)
            m2 = tmp_F[:, :, i_B, j_B] # F(|i_B><j_B|)
            res[:, i_B, i_A, j_B, j_A] = kron(m1, m2) |> vec
        end
    end

    return reshape(res, (m_E * m_F, n_E * n_F))
end


"""
    phi_VT(α)

The state that Alice prepares in the entanglement based version of the single-round bound.
"""
function phi_VT(α::Number)
    s = exp(-2 * α^2)
    phi0 = [1, 0]
    phi1 = [s, √(1 - s^2)]

    z = [1, 0]
    o = [0, 1]

    phi = (kron(z, phi0) + kron(o, phi1)) / √(2)
    return phi * phi'
end


"""
    optimize_entropy(params; λ_0)

Optimize the smooth min-entropy (by optimizing the min-tradeoff function).
"""
function optimize_entropy(params::ProtocolParameters; λ_0=[1e-3, 1e-3, -1e-3])
    # Initialize the protocol parameters
    α = params.α
    n = params.n
    γ = params.γ
    p0 = params.p0

    eps = 1e-12
    sec_params = SecurityParameters(eps, eps, eps, 4e-3)

    function f(λ)
        # Build the min-tradeoff function
        c_λ = min_tradeoff(α, γ, λ)
        trdf = MinTradeoff(c_λ, λ)

        # Update the choice of min-tradeoff function
        prot_params = ProtocolParameters(; n, γ, α, p0, trdf)

        # Evaluate the EAT using the new choice of min-tradeoff function
        min_ent_eat = apply_EAT(prot_params, sec_params)

        # Subtract the remaining penalty terms
        min_ent = min_ent_eat - (ceil(log2(1 / sec_params.eps_EC)) + 2 * log2(1 / sec_params.eps_PA) + 2) / n
        return -min_ent
    end

    # Optimize the min-tradeoff function
    res = Optim.optimize(
        f,
        λ_0,
        NelderMead(),
        Optim.Options(; f_calls_limit=400),
    )
    @show res
    @show -Optim.minimum(res)

    return -Optim.minimum(res), Optim.minimizer(res)
end


"""
    bisect(f, x1, x2)

Find a root of `f` in the interval `[x1,x2]` using bisection.
"""
function bisect(f::Function, x1::Number, x2::Number)
    f1 = f(x1)
    f2 = f(x2)

    @assert f1 * f2 <= 0
    if f1 > f2
        f1, f2 = f2, f1
        x1, x2 = x2, x1
    end

    while true
        mid = (x1 + x2) / 2
        if abs(x2 - x1) < 1e-15
            return mid
        end

        f_mid = f(mid)
        if f_mid < 0
            x1 = mid
            f1 = f_mid
        else
            x2 = mid
            f2 = f_mid
        end
    end
end


"""
    entropy_production(prot_params, sec_params, target_eps::Float64)

The possible per-round entropy production for some given completeness parameter `target_eps`.
"""
function entropy_production(
    prot_params::ProtocolParameters,
    sec_params::SecurityParameters,
    target_eps::Float64, # The target completeness parameter
)
    props = min_tradeoff_props(prot_params)
    n = prot_params.n

    function f(δ)
        err_PE = exp(-n * (δ^2 / 2) / (props.var + (props.max - props.min) * δ / 3))
        return sec_params.eps_EC_com + err_PE - target_eps
    end

    @assert sec_params.eps_EC_com < target_eps
    δ = bisect(f, 0, 1)
    trdf = prot_params.trdf
    ca_hon = trdf.c_λ + dot(trdf.λ, prot_params.p0)
    return ca_hon - δ
end


"""
    apply_EAT(prot_params, sec_params)

Apply the EAT to compute the smooth min entropy of our protocol.
"""
function apply_EAT(
    prot_params::ProtocolParameters,
    sec_params::SecurityParameters
)
    n = prot_params.n
    p_succ = 2sec_params.eps_s + sec_params.eps_PA

    d_A = 3
    props = min_tradeoff_props(prot_params)

    g(ε) = log2(2 / ε^2)

    # For improved numerical accuracy we define β = α - 1
    function f(β)
        V = log2(1 + 2d_A^2) + √(2 + props.var)

        x = 2log(d_A) + props.max - props.min_sigma
        K = (1 - β)^3 / (6log(2) * (1 - 2β)^3) * 2^(β / (1 - β) * x)
        K *= (log(2) * x + log(1 + 2^(-x) * ℯ^2))^3

        s0 = entropy_production(prot_params, sec_params, 1e-2)
        s1 = β / (1 - β) * log(2) / 2 * V^2
        s2 = (g(sec_params.eps_s) + (1 + β) * log2(1 / p_succ)) / (n * β)
        s3 = (β / (1 - β))^2 * K
        return -(s0 - s1 - s2 - s3)
    end

    res = Optim.optimize(f, 1e-10, 0.1)
    # @show Optim.minimizer(res)
    return -Optim.minimum(res)
end


"""
    mmt_Bob(v)

Bob's POVM indexed by `v`.
"""
function mmt_Bob(v::Int)
    oo = [0, 0, 0, 1]
    if v == 1
        phi_plus = [0, 1, 1, 0] / sqrt(2)
        return phi_plus * phi_plus' + 0.5 * oo * oo'
    elseif v == 2
        phi_minus = [0, 1, -1, 0] / sqrt(2)
        return phi_minus * phi_minus' + 0.5 * oo * oo'
    elseif v == 3
        # Vacuum
        zz = [1, 0, 0, 0]
        return zz * zz'
    end
end


"""
    mmt(u::Int, v::Int)

The combined POVM between Alice and Bob.
"""
function mmt(u::Int, v::Int)
    id = Matrix(I, 2, 2)
    vec = id[:, u]
    mat_A = vec * vec'
    mat_B = mmt_Bob(v)

    return kron(mat_A, mat_B)
end


"""
    constraint_mmt(i::Int)

The measurements used for the statistical check.
"""
function constraint_mmt(i::Int)
    if i == 1
        # Correct outcome
        return mmt(1, 1) + mmt(2, 2)
    elseif i == 2
        # Incorrect outcome
        return mmt(1, 2) + mmt(2, 1)
    elseif i == 3
        # Inconclusive outcome
        return mmt(1, 3) + mmt(2, 3)
    else
        @error "Index out of bounds"
    end
end


"""
    key_rates(build_params::Function)

Compute the key rates for the protocol whose parameters are given through `build_params`.
"""
function key_rates(build_params::Function)
    df_in = CSV.read("data/opt_alphas.csv", DataFrame)
    alphas = Vector(df_in.alpha_opt)
    etas = Vector(df_in.eta)

    min_ents = Float64[]
    grads = Vector{Float64}[]
    for (α, η) in zip(alphas, etas)
        params = build_params(; α, η)
        min_ent, grad = optimize_entropy(params)
        push!(min_ents, min_ent)
        push!(grads, grad)
    end

    x = log10(n) |> floor |> Int
    df_out = DataFrame((
        eta=df_in.eta,
        alpha_opt=df_in.alpha_opt,
        min_ent=min_ents,
        lambda=grads
    ))
    CSV.write("data/opt_grads_n=1e$x.csv", df_out)
end


"""
    optimize_alphas(constraint_prob::Function, fname="data/opt_alphas.csv")

Find the amplitude `alpha` that optimizes the key rate.
"""
function optimize_alphas(constraint_prob::Function, fname="data/opt_alphas.csv")
    etas = 10 .^ (-LinRange(3, 0, 50))

    opt_alphas = Float64[]
    ents = Float64[]
    for η in etas
        function f(α)
            stats = [constraint_prob(α, η, i) for i in 1:3]
            ent, _ = compute_grad(α; stats)
            return -ent
        end

        res = Optim.optimize(f, 0.1, 0.9)
        @show η, Optim.minimizer(res)

        push!(ents, -Optim.minimum(res))
        push!(opt_alphas, Optim.minimizer(res))
    end

    df = DataFrame((eta=etas, alpha_opt=opt_alphas, rate=ents))
    CSV.write(fname, df)
    return nothing
end


"""
    read_file(fname)

Utility function to extract information from previous optimizations.
"""
function read_file(fname::AbstractString)
    df = CSV.read(fname, DataFrame)

    grads = Vector{Float64}[]
    for s in Vector(df.lambda)
        s = strip(s, ('[', ']'))
        els = split(s, ',')
        out::Vector{Float64} = parse.(Float64, els)
        push!(grads, out)
    end

    return df, grads
end

