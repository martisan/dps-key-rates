module KeyRates

using LinearAlgebra
using JuMP
using Mosek
using MosekTools
using DataFrames
using CSV
using FastGaussQuadrature
using Makie
using CairoMakie
using ElectronDisplay
using QuantumInformation


function quad_weights(m::Int)
    t, w = gaussradau(m)
    (1 .- reverse(t)) / 2, reverse(w) / 2
end


function compute_entropy(α; η=1.0, m::Int=11)
    # The dimensions
    dim_A = 2
    dim_B = 2 * 2 # Dimensions in the Choi Isomorphism
    dim_AB = dim_A * dim_B

    model = Model(optimizer_with_attributes(Mosek.Optimizer, "QUIET" => true))
    ts, ws = quad_weights(m)


    ##### Build the variables #####
    # The choi state
    @variable(model, rho_cj[1:dim_AB, 1:dim_AB], PSD)
    rho_cj = convert(Matrix{AffExpr}, rho_cj)

    # Compute the channel from the choi state
    E = inv_choi_isom(convert(Matrix{AffExpr}, rho_cj), dim_A, dim_B)

    # Apply the channel
    phi_in = phi_VT(α)
    chan = channel_kron(Matrix(I, 2^2, 2^2), E)
    rho = mat(chan * vec(phi_in))

    zetas = Matrix{AffExpr}[]
    nus = Matrix{AffExpr}[]
    thetas = Matrix{AffExpr}[]
    for i in 1:m, a in 1:2
        M, N = size(rho)

        Gamma1 = @variable(model, [1:2M, 1:2N], PSD)
        Gamma2 = @variable(model, [1:2M, 1:2N], PSD)
        @constraint(model, Gamma1[1:M, 1:N] .== rho)
        @constraint(model, Gamma2[1:M, 1:N] .== rho)
        @constraint(model, Gamma1[1:M, (N+1):end] .== Gamma2[(M+1):end, 1:N])

        zeta_i = Gamma1[1:M, (N+1):end]
        push!(zetas, zeta_i)

        nu_i = Gamma1[(M+1):end, (N+1):end]
        push!(nus, nu_i)

        theta_i = Gamma2[(M+1):end, (N+1):end]
        push!(thetas, theta_i)
    end
    zetas = reshape(zetas, m, 2)
    nus = reshape(nus, m, 2)
    thetas = reshape(thetas, m, 2)


    ##### Build the objective #####
    obj = 0
    mmt_ops = [mmt(1, 1) + mmt(1, 2), mmt(2, 1) + mmt(2, 2)]
    op_A_tot = sum(mmt_ops)
    for i in 1:m, a in eachindex(mmt_ops)
        op_A = mmt_ops[a]
        op = op_A * rho + op_A' * (zetas[i, a] + zetas[i, a]' + (1 - ts[i]) * nus[i, a]) + ts[i] * op_A_tot * thetas[i, a]
        obj += ws[i] * tr(op) / (ts[i] * log(2))
    end
    @objective(model, Min, obj)


    ##### Add the constraints #####
    # The constraints on the choi matrix
    @constraint(model, tr(rho_cj) == 1)

    # The non-signaling constraints on the choi matrix
    dims = [2, 2, 2]
    lhs = ptrace(1 * rho_cj, dims, 2)
    rhs = kron(mixed(2), ptrace(1 * rho_cj, dims, [1, 2]))
    @constraint(model, lhs .== rhs)


    # The measurement constraints
    for u in 1:2, v in 1:3
        G = mmt(u, v)
        @constraint(model, tr(G * rho) == prob(α, η, u, v))
    end

    # Solve the SDP
    optimize!(model)
    return dual_objective_value(model)
end


"""
    inv_choi_isom(ρ, dim_A, dim_B)

Apply the inverse of the Choi-Jamiołkowski isomorphism.
"""
function inv_choi_isom(rho::Matrix{T}, dim_A::Int, dim_B::Int) where {T}
    m, n = size(rho)
    @assert m == n
    @assert dim_A * dim_B == m

    tmp1 = reshape(rho, (dim_B, dim_A, dim_B, dim_A))
    tmp2 = PermutedDimsArray(tmp1, [1, 3, 2, 4])
    return dim_A * reshape(tmp2, (dim_B^2, dim_A^2))
end


"""
    mat(v::Vector)

Reshape a vector into a square matrix. This is the inverse to `vec`.
"""
function mat(v::Vector{T}) where {T}
    n = length(v)
    m = isqrt(n)
    @assert m^2 == n

    return reshape(v, (m, m))
end


"""
    channel_kron(E, F)

Tensor product of two channels.
"""
function channel_kron(E::Matrix{T}, F::Matrix{U}) where {T,U}
    m_E, n_E = size(E)
    dim_A1, dim_A2 = isqrt(n_E), isqrt(m_E)
    @assert m_E == dim_A2^2 && n_E == dim_A1^2
    tmp_E = reshape(E, (dim_A2, dim_A2, dim_A1, dim_A1))

    m_F, n_F = size(F)
    dim_B1, dim_B2 = isqrt(n_F), isqrt(m_F)
    @assert m_F == dim_B2^2 && n_F == dim_B1^2
    tmp_F = reshape(F, (dim_B2, dim_B2, dim_B1, dim_B1))

    # Build the tensored channel
    res = zeros(promote_type(T, U), m_E * m_F, dim_B1, dim_A1, dim_B1, dim_A1)
    for i_A in 1:dim_A1, j_A in 1:dim_A1
        for i_B in 1:dim_B1, j_B in 1:dim_B1
            m1 = tmp_E[:, :, i_A, j_A] # E(|i_A><j_A|)
            m2 = tmp_F[:, :, i_B, j_B] # F(|i_B><j_B|)
            res[:, i_B, i_A, j_B, j_A] = kron(m1, m2) |> vec
        end
    end

    return reshape(res, (m_E * m_F, n_E * n_F))
end


"""
    phi_VT(α)

The state that Alice prepares in the entanglement based version of the single-round bound.
"""
function phi_VT(α::Number)
    s = exp(-2 * α^2)
    phi0 = [1, 0]
    phi1 = [s, √(1 - s^2)]

    z = [1, 0]
    o = [0, 1]

    phi = (kron(z, phi0) + kron(o, phi1)) / √(2)
    return phi * phi'
end


"""
    mmt_Bob(v)

Bob's POVM indexed by `v`.
"""
function mmt_Bob(v::Int)
    oo = [0, 0, 0, 1]
    if v == 1
        phi_plus = [0, 1, 1, 0] / sqrt(2)
        return phi_plus * phi_plus' + 0.5 * oo * oo'
    elseif v == 2
        phi_minus = [0, 1, -1, 0] / sqrt(2)
        return phi_minus * phi_minus' + 0.5 * oo * oo'
    elseif v == 3
        # Vacuum
        zz = [1, 0, 0, 0]
        return zz * zz'
    end
end


"""
    mmt(u::Int, v::Int)

The combined POVM between Alice and Bob.
"""
function mmt(u::Int, v::Int)
    id = Matrix(I, 2, 2)
    vec = id[:, u]
    mat_A = vec * vec'
    mat_B = mmt_Bob(v)

    return kron(mat_A, mat_B)
end


"""
    mixed(dim::Int)

The fully mixed state of dimension `dim`.
"""
mixed(dim::Int) = Matrix(I, dim, dim) / dim


"""
    prob(α, η, u::Int, v::Int)

The expected statistics of the protocol for a given transmittance.
"""
function prob(α, η, u::Int, v::Int)
    p_inc = exp(-2 * η * α^2)
    if u == 1 && v == 1
        return (1 - p_inc) / 2
    elseif u == 2 && v == 2
        return (1 - p_inc) / 2
    elseif u == 1 && v == 3
        return p_inc / 2
    elseif u == 2 && v == 3
        return p_inc / 2
    else
        return 0.0
    end
end


α = 0.4
@show compute_entropy(α)

end # module KeyRates
