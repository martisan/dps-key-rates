"""
Finite size key rates for the relativistic protocol.
"""
module KeyRates

include("../common/finite_key_rates.jl")

using LinearAlgebra
using DataFrames
using CSV


"""
    prob(α, η, u::Int, v::Int)

The expected statistics of the protocol for a given transmittance.
"""
function prob(α, η, u::Int, v::Int)
    p_inc = exp(-2 * η * α^2)
    if u == 1 && v == 1
        return (1 - p_inc) / 2
    elseif u == 2 && v == 2
        return (1 - p_inc) / 2
    elseif u == 1 && v == 3
        return p_inc / 2
    elseif u == 2 && v == 3
        return p_inc / 2
    else
        return 0.0
    end
end


"""
    constraint_prob(α, η, i::Int)

The expected values of the statistical check for a given transmittance.
"""
function constraint_prob(α, η, i::Int)
    if i == 1
        return prob(α, η, 1, 1) + prob(α, η, 2, 2)
    elseif i == 2
        return prob(α, η, 1, 2) + prob(α, η, 2, 1)
    elseif i == 3
        return prob(α, η, 1, 3) + prob(α, η, 2, 3)
    else
        @error "Index out of bounds"
    end
end


# The testing frequencies for the different number of rounds.
gammas = Dict(
    Int(1e15) => 8e-3,
    Int(1e12) => 2e-2,
    Int(1e10) => 4e-2,
    Int(1e8) => 7e-2,
)


function key_rates(x::Int)
    n = Int(10^x)
    ifname = "data/opt_alphas.csv"
    data = CSV.read(ifname, DataFrame)

    rates = Float64[]
    grads::Vector{Vector{Float64}} = []
    for i in 1:length(data.eta)
        η = data.eta[i]
        α = data.alpha_opt[i]
        λ = [0.0, 0.0, 0.0]

        γ = gammas[n]
        p0 = constraint_prob.(α, η, 1:3)
        trdf = MinTradeoff(0, λ)
        prot_params = ProtocolParameters(; n, γ, α, p0, trdf)
        rate, grad = optimize_entropy(prot_params)

        @show η, rate
        push!(rates, rate)
        push!(grads, grad)
    end

    ofname = "data/opt_grads_n=1e$x.csv"
    df_out = DataFrame((eta=data.eta, alpha_opt=data.alpha_opt, min_ent=rates, lambda=grads))
    CSV.write(ofname, df_out)
end


# First we need to find the optimal amplitude α of the laser light.
# This line can be commented out if the file `data/opt_alphas.csv` already exists.
optimize_alphas(constraint_prob)

for x in [8, 10, 12, 15]
    key_rates(x)
end

end # module KeyRates

