This repository contains the code for computing key rates of the relativistic protocol
- The file `key_rates.jl` is used to compute asymptotic key_rates without QBER. This is not used for any plot in the paper but may serve as a good introduction into how the key rate calculations are implemented.
- The file `key_rates_qber.jl` is used to compute asymptotic key rates in the presence of QBER.
- The file `finite_key_rates.jl` contains the code for computing key rates specific to the relativistic protocol (see also `common/finite_key_rates.jl`).
