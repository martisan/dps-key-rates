"""
Compute upper-bounds on the tolerable noise.

Based on https://journals.aps.org/pra/pdf/10.1103/PhysRevA.77.052321
"""
module Attack

using CairoMakie
using ElectronDisplay
using Optim
using DataFrames
using CSV
using LaTeXStrings
using LinearAlgebra


"""
    AttackParams

The parameters of Eve's attack.
"""
struct AttackParams
    M_min::Int
    M_max::Int
    d::Int
    q::Float64
    p_succ::Float64
end


"""
    AttackParams(; M_min, M_max, q, alpha)

Determine Eve's attack parameters for some given alpha.
"""
function AttackParams(; M_min::Int, M_max::Int, q::Float64, alpha::Float64)
    @assert M_min < M_max

    p_inc = exp(-2 * alpha^2)
    p_succ = 1 - p_inc
    d = 0
    return AttackParams(M_min, M_max, d, q, p_succ)
end


"""
    p_signal(k, params)

Compute the probability to obtain k successive time conclusive mmt. outcomes.
"""
function p_signal(k::Int, params::AttackParams)
    M_min = params.M_min
    M_max = params.M_max
    p_succ = params.p_succ
    q = params.q

    if k == M_min
        return q * p_succ^M_min * (1 - p_succ)
    elseif M_min < k < M_max
        return p_succ^k * (1 - p_succ)
    elseif k == M_max
        return p_succ^M_max
    else
        return 0.0
    end
end


"""
    gain(params)

Compute the gain (N_clicks/N) of the attack.
"""
function gain(params::AttackParams)
    M_min = params.M_min
    M_max = params.M_max
    d = params.d
    q = params.q
    p_succ = params.p_succ

    N_clicks = (q + (1 - q) * p_succ) * p_succ^M_min
    N = (d * (q + (1 - 2q) * p_succ - (1 - q) * p_succ^2) * p_succ^M_min - p_succ^(M_max + 1) + 1) / (1 - p_succ)
    return N_clicks / N
end


"""
    qber(params)

Compute the QBER (quantum bit error rate) of the attack.
"""
function qber(params::AttackParams)
    M_min = params.M_min
    M_max = params.M_max
    q = params.q
    p_succ = params.p_succ

    # function As(k)
    #     coeffs = Float64[]
    #     for n in 1:k
    #         coeff = (1 / sqrt(2))^(k - 1) * sqrt(binomial(k - 1, n - 1))
    #         push!(coeffs, coeff)
    #     end
    #     return coeffs
    # end

    function As(k)
        K = diagm(1 => ones(k - 1), -1 => ones(k - 1))
        F = eigen(K)
        i = argmax(F.values)
        return F.vectors[:, i]
    end

    function err(k)
        coeffs = As(k)
        x = abs(coeffs[1])^2 + abs(coeffs[k])^2
        for n in 1:(k-1)
            x += abs(coeffs[n+1] - coeffs[n])^2
        end
        return 0.25 * x
    end

    N_clicks = (q + (1 - q) * p_succ) * p_succ^M_min
    N_errors = 0.0
    for k in M_min:M_max
        N_errors += p_signal(k, params) * err(k)
    end

    return N_errors / N_clicks
end


"""
    build_params(M_avg, alpha)

Build the attack parameters for interpolated `M_avg`.
"""
function build_params(M_avg::Float64, alpha::Float64)
    M_min = floor(M_avg) |> Int
    M_max = 25
    q = 1 - (M_avg - M_min)
    return AttackParams(; M_min, M_max, q, alpha=alpha)
end


function make_plot(alpha=0.4)
    Ms = LinRange(1, 24, 300)
    gains = Float64[]
    qbers = Float64[]
    for M in Ms
        params = build_params(M, alpha)
        push!(gains, gain(params))
        push!(qbers, qber(params))
    end
    # etas = -log.(1 .- gains) / alpha^2
    etas = gain_to_eta.(gains, alpha)

    fig = Figure()
    fig[1, 1] = ax = Axis(fig; xlabel=L"\eta", ylabel="QBER", xscale=log10)
    lines!(ax, etas, qbers; linestyle="--", label="Without rel. constraints")

    df_out = DataFrame((eta=etas, qber=qbers))
    CSV.write("data/bound_curty.csv", df_out)

    ifname = "data/threshold_qbers.csv"
    df = CSV.read(ifname, DataFrame)
    gains = 1 .- exp.(-df.eta * alpha^2)
    scatter!(ax, df.eta, df.qber; label="ours")
    lines!(ax, df.eta, df.qber, linestyle="--")

    axislegend(ax; position=:lt)
    xlims!(ax, 1e-5, 1e-1)
    ylims!(ax, 0, 0.15)
    electrondisplay(fig)
end


"""
    gain_to_eta(G, alpha)

Convert gain to transmittance(for some given alpha).
"""
function gain_to_eta(G, alpha)
    eta = -log(1 - G) / alpha^2
    return eta
end


alpha = 0.4
params = AttackParams(; M_min=7, M_max=25, q=1.0, alpha)
@show gain(params)
@show qber(params)

make_plot()

end # module Attack
