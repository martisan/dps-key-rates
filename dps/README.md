This repository contains the code for computing key rates of the DPS protocol
- The file `finite_key_rates.jl` contains the code for computing key rates specific to DPS (see also `common/finite_key_rates.jl`).
- The file `attack.jl` implements the attack of Curty et al. Available [here](https://arxiv.org/abs/0803.1473).
- The file `threshold_qbers.jl` computes the threshold QBER for the DPS protocol. This is used to compare against the attack of Curty et al.
