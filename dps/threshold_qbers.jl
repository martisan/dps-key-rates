"""
Find the threshold QBER (quantum bit error rate) of the DPS protocol.

This is done using a bisection method and numerical key-rate computation
"""
module ThresholdQBER

using LinearAlgebra
using JuMP
using MathOptInterface
using Mosek
using MosekTools
using QuantumInformation
using FastGaussQuadrature
using Makie
using DataFrames
using CSV
using CairoMakie
using LaTeXStrings
using ElectronDisplay


"""
    quad_weights(m)

Weights of Gauss-Radau quadrature of order `m`.
"""
function quad_weights(m::Int)
    t, w = gaussradau(m)
    (1 .- reverse(t)) / 2, reverse(w) / 2
end


"""
    compute_entropy(alpha, q; eta=1.0, m=20)

Compute the entropy H(A|EI) for some QBER `q` and transmittance `eta`.
"""
function compute_entropy(alpha, q; eta=1.0, m::Int=20)
    # The dimensions
    dim_P = 2
    dim_S = dim_R = 2
    dim_A = 2
    dim_B = dim_S * dim_R # Dimensions in the Choi Isomorphism
    dim_AB = dim_A * dim_B

    model = Model(optimizer_with_attributes(Mosek.Optimizer, "QUIET" => true))
    ts, ws = quad_weights(m)


    ##### Build the variables #####
    # The Choi state
    @variable(model, rho_cj[1:dim_AB, 1:dim_AB], PSD)
    rho_cj = convert(Matrix{AffExpr}, rho_cj)

    # Compute the channel superoperator from the Choi state
    E = inv_choi_isom(convert(Matrix{AffExpr}, rho_cj), dim_A, dim_B)

    # Apply the channel
    phi_in = phi_PS(alpha)
    chan = channel_kron(Matrix(I, 2^2, 2^2), E)
    rho = mat(chan * vec(phi_in))

    zetas = Matrix{AffExpr}[]
    etas = Matrix{AffExpr}[]
    thetas = Matrix{AffExpr}[]
    for i in 1:m, a in 1:2
        M, N = size(rho)

        Gamma1 = @variable(model, [1:2M, 1:2N], PSD)
        Gamma2 = @variable(model, [1:2M, 1:2N], PSD)
        @constraint(model, Gamma1[1:M, 1:N] .== rho)
        @constraint(model, Gamma2[1:M, 1:N] .== rho)
        @constraint(model, Gamma1[1:M, (N+1):end] .== Gamma2[(M+1):end, 1:N])

        zeta_i = Gamma1[1:M, (N+1):end]
        push!(zetas, zeta_i)

        eta_i = Gamma1[(M+1):end, (N+1):end]
        push!(etas, eta_i)

        theta_i = Gamma2[(M+1):end, (N+1):end]
        push!(thetas, theta_i)
    end
    zetas = reshape(zetas, m, 2)
    etas = reshape(etas, m, 2)
    thetas = reshape(thetas, m, 2)


    ##### Build the objective #####
    obj = 0
    mmt_ops = [mmt(u, v) for u in 1:2, v in 1:3]
    for i in 1:m, a in 1:2
        op_A = mmt_ops[a, 1] + mmt_ops[a, 2]
        op_A_tot = mmt_ops[1, 1] + mmt_ops[1, 2] + mmt_ops[2, 1] + mmt_ops[2, 2]
        op = op_A * rho + op_A * (zetas[i, a] + zetas[i, a]' + (1 - ts[i]) * etas[i, a]) + ts[i] * op_A_tot * thetas[i, a]
        obj += ws[i] * tr(op) / (ts[i] * log(2))
    end
    @objective(model, Min, obj)


    ##### Add the constraints #####
    # The constraints on the Choi matrix
    @constraint(model, tr(rho_cj) == 1)

    # The non-signaling constraints on the Choi matrix
    dims = [2, dim_S, dim_R]
    lhs = ptrace(rho_cj, dims, 2)
    rhs = kron(mixed(2), ptrace(rho_cj, dims, [1, 2]))
    @constraint(model, lhs .== rhs)

    # Enforce the measurement constraints
    for i in 1:3
        G = mmt(i)
        @constraint(model, tr(G * rho) == prob(alpha, eta, q, i))
    end

    # Solve the SDP
    optimize!(model)

    if termination_status(model) != MathOptInterface.OPTIMAL
        @show termination_status(model)
    end

    return dual_objective_value(model)
end


"""
    inv_choi_isom(ρ, dim_A, dim_B)

Apply the inverse of the Choi-Jamiołkowski isomorphism
"""
function inv_choi_isom(rho::Matrix{T}, dim_A::Int, dim_B::Int) where {T}
    m, n = size(rho)
    @assert m == n
    @assert dim_A * dim_B == m

    tmp1 = reshape(rho, (dim_B, dim_A, dim_B, dim_A))
    tmp2 = PermutedDimsArray(tmp1, [1, 3, 2, 4])
    return dim_A * reshape(tmp2, (dim_B^2, dim_A^2))
end


"""
    mat(v)

Convert vector to matrix by stacking it into columns.
"""
function mat(v::Vector{T}) where {T}
    n = length(v)
    m = isqrt(n)
    @assert m^2 == n

    return reshape(v, (m, m))
end


"""
    channel_kron(E, F)

Construct the tensor product of two channels (given as superoperators).
"""
function channel_kron(E::Matrix{T}, F::Matrix{U}) where {T,U}
    m_E, n_E = size(E)
    dim_A1 = isqrt(n_E)
    dim_A2 = isqrt(m_E)
    @assert m_E == dim_A2^2 && n_E == dim_A1^2
    tmp_E = reshape(E, (dim_A2, dim_A2, dim_A1, dim_A1))

    m_F, n_F = size(F)
    dim_B1 = isqrt(n_F)
    dim_B2 = isqrt(m_F)
    @assert m_F == dim_B2^2 && n_F == dim_B1^2
    tmp_F = reshape(F, (dim_B2, dim_B2, dim_B1, dim_B1))

    # Build the tensored channel
    res = zeros(promote_type(T, U), m_E * m_F, dim_B1, dim_A1, dim_B1, dim_A1)
    for i_A in 1:dim_A1, j_A in 1:dim_A1
        for i_B in 1:dim_B1, j_B in 1:dim_B1
            m1 = tmp_E[:, :, i_A, j_A] # E(|i_A><j_A|)
            m2 = tmp_F[:, :, i_B, j_B] # F(|i_B><j_B|)
            res[:, i_B, i_A, j_B, j_A] = kron(m1, m2) |> vec
        end
    end

    return reshape(res, (m_E * m_F, n_E * n_F))
end


"""
    phi_PS(alpha)

The input to Eve's attack (in the entanglement based version of the protocol).
"""
function phi_PS(alpha)
    s = exp(-2 * alpha^2)
    phi0 = [1, 0]
    phi1 = [s, √(1 - s^2)]

    z = [1, 0]
    o = [0, 1]

    phi = (kron(z, phi0) + kron(o, phi1)) / √(2)
    return phi * phi'
end

"""
    mmt_Bob(v)

Bob's POVM for his outcome indexed by `v`.
"""
function mmt_Bob(v::Int)
    oo = [0, 0, 0, 1]
    if v == 1
        phi_plus = [0, 1, 1, 0] / √(2)
        return phi_plus * phi_plus' + 0.5 * oo * oo'
    elseif v == 2
        phi_minus = [0, 1, -1, 0] / √(2)
        return phi_minus * phi_minus' + 0.5 * oo * oo'
    elseif v == 3
        # Vacuum
        zz = [1, 0, 0, 0]
        return zz * zz'
    end
end


"""
    mmt(u, v)

The combined POVMs between Alice and Bob.
"""
function mmt(u::Int, v::Int)
    res = Array{Matrix{Float64}}(undef, 2, 3)

    id = Matrix(I, 2, 2)
    vec = id[:, u]
    mat_A = vec * vec'
    mat_B = mmt_Bob(v)

    return kron(mat_A, mat_B)
end


"""
    mmt(i)

The post-processed measurement used for parameter estimation.
"""
function mmt(i::Int)
    if i == 1
        # Correct outcome
        return mmt(1, 1) + mmt(2, 2)
    elseif i == 2
        # Incorrect outcome
        return mmt(1, 2) + mmt(2, 1)
    elseif i == 3
        # Inconclusive outcome
        return mmt(1, 3) + mmt(2, 3)
    else
        @error "Index out of bounds"
    end
end


"""
    mixed(dim)

Construct the fully mixed state of dimension `dim`.
"""
mixed(dim::Int) = Matrix(I, dim, dim) / dim


"""
    prob(alpha, eta, q, i)

Compute the expected parameter estimation statistics for a given amplitude `alpha`,
transmittance `eta` and QBER `q`.
"""
function prob(alpha, eta, q, i::Int)
    p_inc = exp(-eta * alpha^2)
    if i == 1
        return (1 - q) * (1 - p_inc)
    elseif i == 2
        return q * (1 - p_inc)
    elseif i == 3
        return p_inc
    else
        @error "Index out of bounds"
    end

    # if i == 1
    #     return prob(alpha, q, 1, 1) + prob(alpha, q, 2, 2)
    # elseif i == 2
    #     return prob(alpha, q, 1, 2) + prob(alpha, q, 2, 1)
    # elseif i == 3
    #     return prob(alpha, q, 1, 3) + prob(alpha, q, 2, 3)
    # else
    #     @error "Index out of bounds"
    # end
end

"""
    prob(alpha, eta, q, u, v)

Compute the expected measurement outcomes for a given amplitude `alpha`,
transmittance `eta` and QBER `q`.
"""
function prob(alpha, eta, q, u::Int, v::Int)
    p_inc = exp(-eta * alpha^2)
    if u == 1
        if v == 1
            return (1 - q) * (1 - p_inc) / 2
        elseif v == 2
            return (1 - p_inc) / 2 * q
        elseif v == 3
            return p_inc / 2
        end
    end
    if u == 2
        if v == 1
            return (1 - p_inc) / 2 * q
        elseif v == 2
            return (1 - q) * (1 - p_inc) / 2
        elseif v == 3
            return p_inc / 2
        end
    end
end


"""
    entropy(probs)

The entropy of the distribution `probs`.
"""
function entropy(probs)
    ent = 0.0
    for p in probs
        if iszero(p) || isone(p)
            continue
        end
        ent += -p * log2(p)
    end
    return ent
end


"""
    HAgB(alpha, eta, q)

The conditional entropy H(A|B) (the cost of error correction).
"""
function HAgB(alpha, eta, q)
    p(u, v) = prob(alpha, eta, q, u, v)
    probs = [
        p(1, 1) p(1, 2) 0
        p(2, 1) p(2, 2) 0
        0 0 (p(1, 3)+p(2, 3))
    ]

    HAB = entropy(probs)
    HB = entropy(sum(probs; dims=1))
    return HAB - HB
end


"""
    key_rate(alpha, eta, q)

Compute the key rate of the protocol when instantiated with amplitude `alpha`,
transmittance `eta` and QBER `q`.
"""
function key_rate(alpha, q; eta)
    return compute_entropy(alpha, q; eta) - HAgB(alpha, eta, q)
end


"""
    bisect(f, x1, x2)

Find a root of `f` in the interval `[x1,x2]` using bisection.
"""
function bisect(f::Function, x1::Number, x2::Number)
    f1 = f(x1)
    f2 = f(x2)

    @assert f1 * f2 <= 0
    if f1 > f2
        f1, f2 = f2, f1
        x1, x2 = x2, x1
    end

    while true
        mid = (x1 + x2) / 2
        if abs(x2 - x1) < 1e-10
            return mid
        end

        f_mid = f(mid)
        if f_mid < 0
            x1 = mid
            f1 = f_mid
        else
            x2 = mid
            f2 = f_mid
        end
    end
end


"""
    threshold_qber(alpha, eta)

Find the threshold QBER for amplitude `alpha` and transmittance `eta`.
"""
function threshold_qber(alpha, eta)
    function f(q)
        r = key_rate(alpha, q; eta)
        @show q, r
        return r
    end

    res = bisect(f, 0.0, 0.5)
    @show eta, res
    return res
end


"""
    threshold_qbers(alpha)

Find the threshold QBERs for amplitude `alpha` and save them in a CSV file.
"""
function threshold_qbers(alpha)
    etas = 10 .^ LinRange(-5, -1, 20)
    qbers = Float64[threshold_qber(alpha, eta) for eta in etas]

    ofname = "data/threshold_qbers.csv"
    df = DataFrame((eta=etas, qber=qbers))
    display(df)
    CSV.write(ofname, df)
end

alpha = 0.4
# @show key_rate(alpha, 0.03; eta=1e-5)
threshold_qbers(alpha)

end # module ThresholdQBER
