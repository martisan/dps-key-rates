# Key rate calculations for relativistic and DPS QKD

Most of the finite-size code is shared and can be found in the folder `common`. The code specific to each
protocol can be found in their respective directories.
